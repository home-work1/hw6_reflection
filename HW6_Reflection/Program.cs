﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HW6_Reflection
{
    public class F
    {
        [JsonProperty]
        int i1, i2, i3, i4, i5;
        public static F Get() => new F() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
    }

    public static class SerializationExt
    {
        public static string SerializationToString(this object o) =>
            String.Join("#",
                    from rfl in o.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                    select $"{rfl.Name}:{rfl.GetValue(o)}"
            );

        public static bool DeserializationFromString<T>(this T o, string str) 
        {
            bool res = false;
            foreach (var pr in str.Split('#'))
            {
                var s = pr.Split(':');
                FieldInfo fi = o.GetType().GetField(s[0], BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

                object val = null;

                string tp = fi.FieldType.ToString();
                switch (tp)
                {
                    case "System.Int32":
                        val = Convert.ToInt32(s[1]);
                        res = true;
                        break;
                    default:
                        throw new NotImplementedException($"Преобразование из типа {tp} не поддерживается.");
                }

                fi.SetValue(o, val);
            }
            return res;
        }


    }

    class Program
    {
        static void Main(string[] args)
        {
            F f = F.Get();

            var t1 = DateTime.Now;
            string ser = null;
            for (int i = 0; i < 1_000_000; i++)
            {
                ser = f.SerializationToString();
            }
            var t2 = DateTime.Now;
            Console.WriteLine((t2 - t1).ToString());

            //f = F.Get();

            t1 = DateTime.Now;
            for (int i = 0; i < 1_000_000; i++)
            {
                f.DeserializationFromString(ser);
            }
            t2 = DateTime.Now;
            Console.WriteLine((t2 - t1).ToString());


            t1 = DateTime.Now;
            for (int i = 0; i < 1_000_000; i++)
            {
                ser = Newtonsoft.Json.JsonConvert.SerializeObject(f);
            }
            t2 = DateTime.Now;
            Console.WriteLine((t2 - t1).ToString());

            t1 = DateTime.Now;
            for (int i = 0; i < 1_000_000; i++)
            {
                var fCopy = Newtonsoft.Json.JsonConvert.DeserializeObject<F>(ser);
            }
            t2 = DateTime.Now;
            Console.WriteLine((t2 - t1).ToString());



            Console.ReadKey();
        }
    }
}
